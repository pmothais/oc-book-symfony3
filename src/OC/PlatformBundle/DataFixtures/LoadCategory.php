<?php

namespace OC\PlatformBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use OC\PlatformBundle\Entity\Category;

class LoadCategory extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $names = array(
            "Web development",
            "Mobile development",
            "Graphism",
            "Network"
        );

        foreach ($names as $name) {
            $category = new Category();
            $category->setName($name);

            $manager->persist($category);
        }

        $manager->flush();
    }
}