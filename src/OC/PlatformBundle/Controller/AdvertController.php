<?php

namespace OC\PlatformBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use OC\PlatformBundle\Entity\Advert;
use OC\PlatformBundle\Entity\AdvertSkill;
use OC\PlatformBundle\Entity\Application;
use OC\PlatformBundle\Entity\Image;
use OC\PlatformBundle\Event\PlatformEvents;
use OC\PlatformBundle\Event\MessagePostEvent;
use OC\PlatformBundle\Form\AdvertType;
use OC\PlatformBundle\Form\AdvertEditType;
use \Datetime;

class AdvertController extends Controller
{
    public function indexAction($page)
    {
        if ($page < 1) {
            throw new NotFoundHttpException('The page '. $page .' does not exist');
        }

        $advertsPerPage = 5;

        $adverts = $this->getDoctrine()
            ->getRepository(Advert::class)
            ->getAdverts(($page - 1) * $advertsPerPage, $advertsPerPage)
        ;

        $nbPages = ceil(count($adverts) / $advertsPerPage);

        if ($page > $nbPages) {
            throw new NotFoundHttpException('The page '. $page .' does not exist');
        }

        return $this->render('advert/index.html.twig', array(
            'adverts' => $adverts,
            'nbPages' => $nbPages,
            'page' => $page
        ));
    }

    /**
     * @ParamConverter("advert", options={"mapping": {"id": "id"}})
     */
    public function viewAction(Advert $advert)
    {
        $em = $this->getDoctrine()->getManager();

        if (empty($advert)) {
            throw new NotFoundHttpException(sprintf("The advert %d does not exist", $advert->id));
        }

        $applications = $em->getRepository(Application::class)->findBy(array("advert" => $advert));

        $advertSkills = $em->getRepository(AdvertSkill::class)->findBy(array("advert" => $advert));

        return $this->render('advert/view.html.twig', array(
            'advert' => $advert,
            "applications" => $applications,
            "advertSkills" => $advertSkills
        ));
    }

    /**
     * @Security(
     *      "has_role('ROLE_AUTHOR')",
     *      message="Limited access to the authors only."
     * )
     */
    public function addAction(Request $request)
    {
        $advert = new Advert();

        $form = $this->createForm(AdvertType::class, $advert);    

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {

            $event = new MessagePostEvent($advert->getContent(), $this->getUser());

            $this->get('event_dispatcher')->dispatch(PlatformEvents::POST_MESSAGE, $event);

            $advert->setContent($event->getMessage());

            $em = $this->getDoctrine()->getManager();
            $em->persist($advert);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'The advert has been added');

            return $this->redirectToRoute('oc_platform_view', array('id' => $advert->getId()));
        }

        return $this->render('advert/add.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $advert = $em->getRepository(Advert::class)->find($id);

        if (empty($advert)) {
            throw new NotFoundHttpException(sprintf("The advert %d does not exist", $id));
        }

        $form = $this->createForm(AdvertEditType::class, $advert);
        
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {

            $em->flush();
            
            $request->getSession()->getFlashBag()->add('notice', 'The advert has been updated');

            return $this->redirectToRoute('oc_platform_view', array('id' => $advert->getId()));
        }

        return $this->render('advert/edit.html.twig', array(
            'advert' => $advert,
            'form' => $form->createView()
        ));
    }

    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $advert = $em->getRepository(Advert::class)->find($id);

        if (empty($advert)) {
            throw new NotFoundHttpException(sprintf("The advert %d does not exist", $id));
        }

        $form = $this->get('form.factory')->create();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            
            $em->remove($advert);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'The advert has been removed');

            return $this->redirectToRoute('oc_platform_home');
        }
        
        return $this->render('advert/delete.html.twig',array(
            'advert' => $advert,
            'form' => $form->createView()
        ));
    }

    public function menuAction($limit)
    {
        $em = $this->getDoctrine()->getManager();

        $adverts = $em->getRepository(Advert::class)->findBy(
            array(),
            array('date' => 'desc'),
            $limit,
            0
        );

        return $this->render('advert/menu.html.twig', array(
            'adverts' => $adverts
        ));
    }
}