<?php

namespace OC\PlatformBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Antiflood extends Constraint
{
    public $message = 'You already have posted a message in less than 15 secondes ago. Please wait before posting again.';

    public function validatedBy()
    {
        return 'oc_platform_antiflood';
    }
}