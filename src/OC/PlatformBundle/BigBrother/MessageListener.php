<?php

namespace OC\PlatformBundle\BigBrother;

use OC\PlatformBundle\Event\MessagePostEvent;

class MessageListener
{
    protected $notificator;
    protected $monitoredUsers;

    public function __construct($notificator, $monitoredUsers)
    {
        $this->notificator = $notificator;
        $this->monitoredUsers = $monitoredUsers;
    }

    public function processMessage(MessagePostEvent $event)
    {
        if (in_array($event->getUser()->getUsername(), $this->monitoredUsers)) {
            $this->notificator->notifyByEmail($event->getMessage(), $event->getUser());
        }
    }
}