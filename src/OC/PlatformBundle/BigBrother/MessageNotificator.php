<?php

namespace OC\PlatformBundle\BigBrother;

use Symfony\Component\Security\Core\User\UserInterface;

class MessageNotificator
{
    protected $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function notifyByEmail($message, UserInterface $user)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('New message from a monitored user')
            ->setFrom('admin@mysite.com')
            ->setTo('admin@mysite.com')
            ->setBody('The monitored user '.$user->getUsername().' has posted the following message: '.$message.'.');

        $this->mailer->send($message);
    }
}