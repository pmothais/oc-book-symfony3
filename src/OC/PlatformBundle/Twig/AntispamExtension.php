<?php

namespace OC\PlatformBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use OC\PlatformBundle\Antispam\OCAntispam;

class AntispamExtension extends AbstractExtension
{
    /**
     * @var OCAntispam
     */
    private $ocAntispam;

    public function __construct(OCAntispam $ocAntispam)
    {
        $this->ocAntispam = $ocAntispam;
    }

    public function checkIfArgumentIsSpam($text)
    {
        return $this->ocAntispam->isSpam($text);
    }

    public function getFunctions()
    {
        return array(
            new TwigFunction('checkIfSpam', array($this, 'checkIfArgumentIsSpam'))
        );
    }

    public function getName()
    {
        return 'OCAntispam';
    }
}