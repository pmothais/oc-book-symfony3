<?php

namespace OC\PlatformBundle\Email;

use OC\PlatformBundle\Entity\Application;

class ApplicationMailer
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendNotification(Application $application)
    {
        $message = new \Swift_Message(
            "New application",
            "You have received a new application"
        );

        $message
            ->addTo($application->getAdvert()->getAuthor()) // here author is used but should be email
            ->addFrom("admin@site.com") // invalid email address
        ;

        $this->mailer->send($message);
    }
}