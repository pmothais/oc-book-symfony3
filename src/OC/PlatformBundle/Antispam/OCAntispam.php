<?php

namespace OC\PlatformBundle\Antispam;

class OCAntispam
{
    /**
     * Verify if a text is a spam or not
     * 
     * @param string $text
     * @return bool
     */
    public function isSpam($text)
    {
        return strlen($text) < 50;
    }
}