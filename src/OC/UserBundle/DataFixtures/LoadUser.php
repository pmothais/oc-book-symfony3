<?php

namespace OC\UserBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use OC\UserBundle\Entity\User;

class LoadUser extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $names = array(
            'Bastien',
            "Cédric",
            "Philippe"
        );

        foreach ($names as $name) {
            $user = new user();
            $user->setUsername($name);
            $user->setPassword($name);
            $user->setSalt('');
            $user->setRoles(array('ROLE_USER'));

            $manager->persist($user);
        }

        $manager->flush();
    }
}